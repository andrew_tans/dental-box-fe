import color from 'color';

export const mainColor = '#3C3C3C';

export const defaultPrimaryColor = '#1882F9';
export const darkPrimaryColor = color(defaultPrimaryColor)
  .darken(0.2)
  .toString();

export const defaultAccentColor = '#1882F9';
export const darkAccentColor = color(defaultAccentColor)
  .darken(0.2)
  .toString();

export const primaryBackgroundColor = '#FFFFFF';
export const transparentPrimaryBackgroundColor = color(primaryBackgroundColor)
  .alpha(0)
  .toString();

export const darkBackgroundColor = '#4A4747';
export const transparentDarkBackgroundColor = color(darkBackgroundColor)
  .alpha(0)
  .toString();

export const linearBackgroundSolid = color('#000000')
  .alpha(0.4)
  .toString();

export const linearBackgroundTransparent = color(linearBackgroundSolid)
  .alpha(0)
  .toString();

export const secondaryBackgroundColor = color('#FFFFFF')
  .darken(0.1)
  .toString();

export const lightBackgroundColor = color(defaultPrimaryColor)
  .lighten(0.2)
  .toString();

export const dangerColor = '#F00521';
export const darkDangerColor = color(dangerColor)
  .darken(0.2)
  .toString();

export const accountManagementBackgroundColor = '#E5E5E5';

export const primaryTextColor = '#272727';
export const secondaryTextColor = '#8A8A8A';
export const errorTextColor = '#DB4437';

export const dividerColor = 'rgba(0, 0, 0, 0.12)';
export const borderColor = '#DDD';

export const disabledTextColor = '#BDBDBD';

export const lightInputBackgroundColor = 'rgba(0,0,0,0.04)';
export const lightIconColor = '#DBDBDB';

export const facebookBlueColor = '#3B5998';

export const blueGrey50Color = 'rgba(213,213,213, 0.5)';
export const blueGrey300Color = '#90A4AE';
export const blueGrey700Color = '#455A64';
export const grey900Color = '#212121';
export const greyStrokeColor = '#D5D5D5';

export const whiteTranslucentColor = 'rgba(255, 255, 255, 0.65)';
export const transparentColor = 'rgba(255, 255, 255, 0)';

export const blackColor = '#000';
export const whiteColor = '#FFF';
export const yellowColor = '#EBBD02';
export const orangeColor = '#F57F17';
export const redColor = '#F00521';
export const blueColor = '#2196F3';
export const blackTranslucentColor = 'rgba(17, 17, 17, 0.3)';
