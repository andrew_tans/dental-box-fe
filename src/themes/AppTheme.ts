import * as colors from './colors';

export const FONT_SIZE = {
  H1: 26,
  LARGE: 20,
  MEDIUM: 18,
  REGULAR: 16,
  SMALL: 13,
  SUBTITLE: 12,
};

export const DURATIONS = {
  HOVER_ANIMATION: '0.5s',
};

export const SIZES = {
  BUTTON_RADIUS: '25px',
  BUTTONS_PADDING: '0px 30px',
  SCALE_CONTROL_FONT_SIZE: '12px',
};

export const TOOLBAR_HEIGHT = 64;

export const WEATHER_TAB_HEIGHT = 48;

export const ACTIVE_TEXT_OPACITY = 0.5;

export const defaultBoxShadow =
  '0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12)';

export const innerBoxShadow = 'inset 2px 1px 100px -34px rgba(0,0,0,0.75)';

export const topBoxShadow =
  '0 -2px 2px 0 rgba(0,0,0,0.14), 0 -5px 5px 0 rgba(0,0,0,0.12)';

export const bottomBoxShadow =
  '0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 5px 0 rgba(0,0,0,0.12)';

export const headerGradient =
  'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0))';

export const barGradient =
  'linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0.65) 100%);';

export const cssTextTruncate = css => css`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;


export default colors;
