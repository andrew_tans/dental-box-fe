import React from 'react';
import {connect} from 'react-redux';
import styled, {createGlobalStyle} from 'styled-components';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import Main from './screens/Main/containers/MainScreenWrapper';

createGlobalStyle`
  body {
    font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif !important;
    margin: 0;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    position: fixed;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
      monospace;
  }

  #root {
    height: 100%;
  }
`;

const Wrapper = styled.div`
  height: 100%;
  position: relative;
`;

function App(props) {
  const {authChecking} = props;
  
  return (
    <Wrapper>
      <Switch>
        <Route path="/main" component={Main}/>
        <Redirect to="/main"/>
      </Switch>
    </Wrapper>
  );
}


const mapStateToProps = (state: any) => ({

});

const mapDispatchToProps = (dispatch: any) => ({

});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
