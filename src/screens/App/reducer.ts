import types from './types';

const initialState = {
  isWide: false,
};

export default (state = initialState, action: any) => {
  switch (action.type) {
    case types.SET_APP_WIDTH_MODE:
      return {
        ...state,
        isWide: action.payload.isWide,
      };
    default:
      return state;
  }
};
