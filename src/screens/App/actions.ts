import types from './types';

export const setAppWidthMode = (isWide: boolean) => ({
  type: types.SET_APP_WIDTH_MODE,
  payload: {
    isWide,
  },
});
