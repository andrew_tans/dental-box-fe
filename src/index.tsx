import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux';

import AppScreenWrapper from './App';
import { HashRouter } from 'react-router-dom';
import configureStore from './store/configureStore';

declare global {
  interface Window {
    REDUX_STATE: any;
  }
}

const {store} = configureStore(
  typeof window !== 'undefined' ? window.REDUX_STATE : {},
);
    
const theme = createMuiTheme();

const render = (App: React.ElementType) => {
  const root = document.getElementById('root');

  ReactDOM.hydrate(
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <HashRouter>
          <App />
        </HashRouter>
      </Provider>
    </MuiThemeProvider>,
    root,
  );
};

render(AppScreenWrapper);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
