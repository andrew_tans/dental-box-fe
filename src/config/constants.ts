export const production = false;

export const APP_NAME = 'Dental Box';

export const APP_VERSION = '1.0.0';

export const WIDE_SCREEN_MIN_WIDTH = 768;
export const WIDE_SCREEN_SIDE_WIDTH = 420;
