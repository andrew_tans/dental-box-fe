import {NOT_FOUND, redirect} from 'redux-first-router';
import * as routeTypes from './types';
import { GetState } from '../../store/types';

const onBeforeChange = async (dispatch: any, getState: GetState, {action}: any) => {
  if (action.type === NOT_FOUND) {
    dispatch(redirect({type: routeTypes.MAIN}));
  }
};

const onBackNext = (dispatch: any, getState: GetState) => {
  const state = getState();
  console.log(state, 'onBackNext')
};

export default {
  location: 'nav',
  onBeforeChange,
  onBackNext,
};
