import * as routeTypes from './types';

export default {
  [routeTypes.MAIN]: {
    path: '/',
    thunk: () => console.log('cool'),
  },
};
