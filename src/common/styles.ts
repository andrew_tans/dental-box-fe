import AppTheme from '../themes/AppTheme';
import styled, {css, CSSObject} from 'styled-components';

const MEDIUM_WIDTH = '1280px';
const MEDIA_WIDTH = '768px';

export const media = {
  wide: (args: CSSObject) => css`
    @media (min-width: ${MEDIA_WIDTH}) {
      ${css(args)};
    }
  `,
  medium: (args: CSSObject) => css`
    @media (max-width: ${MEDIUM_WIDTH}) {
      ${css(args)};
    }
  `,
  narrow: (args: CSSObject) => {
    return css`
    @media (max-width: ${MEDIA_WIDTH}) {
      ${css(args)};
    }
  `
  },
};

export const Loader = styled.div`
  border: 5px solid ${AppTheme.secondaryBackgroundColor};
  border-top: 5px solid ${AppTheme.defaultPrimaryColor};
  border-radius: 50%;
  width: 20px;
  height: 20px;
  animation: spin 2s linear infinite;

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
