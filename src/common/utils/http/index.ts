
export {fetchOpts, headerConfig} from './request';

interface Params {
  [key: string]: string | number | boolean;
}

const checkStatus = (response: Response) => {
  const json = response.json();
  if (response.status >= 200 && response.status < 300) {
    return json;
  }
  return json.then(Promise.reject.bind(Promise));
};

export const fetchParams = (params: Params): string => {
  const array = Object.keys(params)
    .filter(key => params[key] !== undefined)
    .map(
      key => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`,
    );

  return `${array.join('&')}`;
};

export const networkRequest = (
  baseUrl: string,
  opts: object,
  params?: string,
) => {
  const endpoint = params ? `${baseUrl}?${params}` : baseUrl;
  return (new Promise((resolve, reject) => {
    fetch(endpoint, opts)
      .then(checkStatus)
      .then(data => {
        resolve(data);
      })
      .catch(err => {
        console.log('Error', err)
        reject(err);
      });
  }));
};

export const localGuid = () => {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  return `LOCAL-${s4()}${s4()}${s4()}${s4()}`;
};

