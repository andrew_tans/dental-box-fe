type FetchOptsParams = {
  method?: string,
  body?: {},
  omitHeaders?: boolean,
  token?: string,
};

export const headerConfig = (
  token?: string,
  omitHeaders: boolean = false,
  contentType: string = 'application/json',
) => {
  if (token && !omitHeaders) {
    return {
      'Content-Type': contentType,
      'x-api-token': token,
    };
  }

  return {
    'Content-Type': 'application/json',
  };
};

export const fetchOpts = ({
  method,
  body,
  omitHeaders = false,
  token,
}: FetchOptsParams) => {
  const opts = {
    method,
    headers: headerConfig(token, omitHeaders),
    credentials: 'omit',
    mode: 'cors',
    cache: 'no-cache',
  };

  if (body) {
    return {
      ...opts,
      body: JSON.stringify(body),
    };
  }

  return opts;
};
