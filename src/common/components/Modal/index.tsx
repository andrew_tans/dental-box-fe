import Dialog from '@material-ui/core/Dialog';
import React from 'react';

interface Props {
  open: boolean;
  onClose: () => unknown;
  onExit: () => unknown;
  classes: any;
  onBackdropClick: () => unknown;
  children: React.ReactNode;
};

function ModalWrapper(props: Props) {
  const {open, onClose, onExit, classes, onBackdropClick, children} = props;

  return (
    <Dialog
      open={open}
      onClose={onClose}
      onExit={onExit}
      onBackdropClick={onBackdropClick}
      classes={classes}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      {children}
    </Dialog>
  );
}

export default ModalWrapper;
