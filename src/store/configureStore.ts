import {
  applyMiddleware,
  combineReducers,
  compose as composeEnhancers,
  createStore,
} from 'redux';
import {connectRoutes} from 'redux-first-router';
import logger from 'redux-logger';
import reduxThunk from 'redux-thunk';

import {production} from '../config/constants';
import routesMap from '../config/routes/map';
import middlewares from './middlewares';
import options from '../config/routes/options';
import reducers from './reducers';
import { DBState } from './types';

const compose = production
  ? composeEnhancers
  : window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || composeEnhancers;

const getAppliedMiddlewares = routeMiddleware => {
  const libMiddlewares = [routeMiddleware, reduxThunk];
  const appliedMiddlewares = [...libMiddlewares, ...[]];

  return !production ? [...appliedMiddlewares, logger] : appliedMiddlewares;
};

export default (preloadedState: DBState, initialEntries?: any) => {
  const {
    reducer: nav,
    middleware: routeMiddleware,
    enhancer: navEnhancer,
    thunk,
  } = connectRoutes(routesMap, {...options, initialEntries});

  const appliedMiddlewares = getAppliedMiddlewares(routeMiddleware);
  const middleware = applyMiddleware(...appliedMiddlewares);

  const store = createStore(
    combineReducers({...reducers, nav}),
    preloadedState,
    compose(navEnhancer, middleware),
  );

  return {store, thunk};
};
