import reducers from './reducers';
import {Dispatch} from 'redux';

export type State<T extends {[key: string]: (...args: any[]) => any}> = {
  [P in keyof T]: ReturnType<T[P]>;
};

export type DBState = State<typeof reducers>;

export type GetState = () => DBState;

export interface Action<P> {
    type: string;
    payload: P;
  }
  
export type DBDispatch = Dispatch<DBState>;
